// @flow

import React from 'react';
import { Nav, NavDropdown, MenuItem, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';


type Props = {
  navigate: (url: string) => void;
}

const Navigation = (props: Props) => {
  return (
    <Nav className="header-navigation">
      <LinkContainer to="/calendar">
        <NavItem eventKey={1}>Kalender</NavItem>
      </LinkContainer>
      <NavDropdown eventKey={2} title="Kliendid" id="basic-nav-dropdown">
        <LinkContainer to="/clients/list">
          <MenuItem eventKey={2.1}>Nimekiri</MenuItem>
        </LinkContainer>
        <LinkContainer to="/clients/add">
          <MenuItem eventKey={2.2}>Lisa uus klient</MenuItem>
        </LinkContainer>
      </NavDropdown>
      <NavDropdown eventKey={2} title="Kohtumised" id="basic-nav-dropdown">
        <LinkContainer to="/meets/list">
          <MenuItem eventKey={2.1}>Nimekiri</MenuItem>
        </LinkContainer>
        <LinkContainer to="/meets/add">
          <MenuItem eventKey={2.2}>Lisa kohtumine</MenuItem>
        </LinkContainer>
      </NavDropdown>
      <NavDropdown eventKey={4} title="Pakkumised" id="basic-nav-dropdown">
        <LinkContainer to="/offers/list">
          <MenuItem eventKey={4.2}>Nimekiri</MenuItem>
        </LinkContainer>
        <LinkContainer to="/offers/add">
          <MenuItem eventKey={4.3}>Lisa uus</MenuItem>
        </LinkContainer>
      </NavDropdown>
      <NavDropdown eventKey={5} title="Tooted" id="basic-nav-dropdown">
        <LinkContainer to="/products/list">
          <MenuItem eventKey={5.2}>Nimekiri</MenuItem>
        </LinkContainer>
        <LinkContainer to="/products/add">
          <MenuItem eventKey={5.3}>Lisa uus</MenuItem>
        </LinkContainer>
      </NavDropdown>
    </Nav>
  );
};

export default Navigation;
