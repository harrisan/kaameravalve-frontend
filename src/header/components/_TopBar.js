// @flow

import React from 'react';

const TopBar = (props: Object) => (
  <div className="header-topbar">
    {Object.keys(props.user).length > 0 &&
      <div>
        <span>Tere {props.user.name}!</span>
        <span onClick={() => props.logout()} className="logout-button">Logout</span>
      </div>
    }
  </div>
);

export default TopBar;
