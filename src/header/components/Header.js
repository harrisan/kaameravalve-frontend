// @flow
import React from 'react';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { withRouter } from 'react-router-dom';
import { Image } from 'react-bootstrap';
import TopBar from './_TopBar';
import Navigation from './_Navigation';
import logo from '../../../static/kaameravalve-logo.png';
import { userDataSelector } from '../../session/selectors';
import { logout } from '../../session/actions';

require('../styles/Header.scss');

type Props = {
  navigate: (url: string) => void;
}


class Header extends React.Component {
  props: Props;

  logout = () => {
    this.props.logout();
  }

  render() {
    return (
      <div className="header container">
        <TopBar user={this.props.user} logout={this.logout} />
        <div className="header-logo">
          <Image src={logo} />
        </div>
        {Object.keys(this.props.user).length != 1 &&
          <Navigation navigate={this.props.navigate} />
        }
      </div>
    );
  }
};

const mapState = state => ({
  user: userDataSelector(state),
});

const mapDispatch = dispatch => ({
  navigate: url => dispatch(push(url)),
  logout: () => dispatch(logout()),
});

export default withRouter(connect(mapState, mapDispatch)(Header));
