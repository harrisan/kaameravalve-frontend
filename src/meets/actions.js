// @flow
import { CALL_API } from 'redux-api-middleware';
import * as t from './actionTypes';

export const getMeets = () => ({
  [CALL_API]: {
    endpoint: `${API_URL}/meets`,
    method: 'GET',
    headers: {},
    types: [
      t.GET_MEETS_REQUEST,
      t.GET_MEETS_SUCCESS,
      t.GET_MEETS_FAILURE,
    ],
  },
});

export const addMeet = (meet: Object) => ({
  [CALL_API]: {
    endpoint: `${API_URL}/meets`,
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(meet),
    types: [
      t.ADD_MEET_REQUEST,
      t.ADD_MEET_SUCCESS,
      t.ADD_MEET_FAILURE,
    ],
  },
});

export const emptySuccessError = () => ({
  type: t.EMPTY_SUCCESS_ERROR,
});
