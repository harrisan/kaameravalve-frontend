import { createSelector } from 'reselect';
import { NAME } from './constants';
import { includes } from '../utils/functions';

const meetsSelector = state => state[NAME];

export const meetsListSelector = createSelector(
  meetsSelector,
  meetsState => meetsState.meets,
);

export const meetsSuccessSelector = createSelector(
  meetsSelector,
  meetState => meetState.meetsSuccess,
);

export const meetsErrorSelector = createSelector(
  meetsSelector,
  meetState => meetState.meetsError,
);

