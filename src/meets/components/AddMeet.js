// @flow

import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import { addMeet, emptySuccessError } from '../actions';
import DateTimePicker from '../../utils/components/DateTimePicker';
import { notEmpty } from '../../utils/validators';
import { meetsSuccessSelector, meetsErrorSelector } from '../selectors';

require('../styles/AddMeet.scss');

type Props = {
  requestError: boolean;
  requestSuccess: boolean;
  addMeet: (d: Object) => void;
  emptySuccessError: () => void;
}

type State = {
  startDate: ?Object;
  clientName: ?string;
  goal: ?string;
  content: ?string;

}

class AddMeet extends React.Component {
  state: State;
  props: Props;

  constructor(props: Props) {
    super(props);

    this.state = {
      startDate: moment(),
      clientName: null,
      goal: null,
      content: null,
    };
  }

  getValidationState = (controlId: string) => {
    const field = this.state[controlId];
    if (field === null) {
      return null;
    }

    return field && field !== '' ? null : 'error';
  }

  handleChange = (e: Event) => {
    if (this.props.requestSuccess || this.props.requestError) {
      this.props.emptySuccessError();
    }

    this.setState({ [e.target.id]: e.target.value });
  }

  handleDateChange = (date) => {
    this.setState({ startDate: date });
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const {
      startDate,
      clientName,
      goal,
      content,
    } = this.state;

    this.props.addMeet({
      startDate: moment(startDate).format('YYYY-MM-DD HH:mm:ss'),
      clientName,
      goal,
      content,
    });
  }

  clearForm = () => {
    this.setState({
      startDate: moment(),
      clientName: null,
      goal: null,
      content: null,
      requestSuccess: false,
      requestError: false,
    });
  }

  isFormvalid = () => (
    this.state.startDate instanceof moment
    && notEmpty(this.state.clientName)
    && notEmpty(this.state.goal)
    && notEmpty(this.state.content)
  );

  render() {
    const { requestSuccess, requestError } = this.props;
    return (
      <div className="add-meet">
        <h2>
          Lisa kohtumine
        </h2>

        <span className="clear-btn" onClick={() => this.clearForm()}>Tühjenda vorm</span>
        <form
          className="add-meet-form"
          id="add-meet-form"
          action=""
          autoComplete="off"
          onSubmit={this.handleSubmit}
        >
          <FormGroup>
            <DateTimePicker
              dateTimeChanged={this.handleDateChange}
              dateTime={this.state.startDate}
              label="Kohtumise kuupäev"
            />
          </FormGroup>

          <FormGroup controlId="clientName" validationState={this.getValidationState('clientName')}>
            <ControlLabel>Kliendi nimi</ControlLabel>
            <FormControl
              type="text"
              name="clientName"
              placeholder="Kliendi nimi"
              onChange={this.handleChange}
              value={this.state.clientName}
            />
          </FormGroup>

          <FormGroup controlId="goal" validationState={this.getValidationState('goal')}>
            <ControlLabel>Eesmärk</ControlLabel>
            <FormControl
              type="text"
              name="goal"
              placeholder="Eesmärk"
              onChange={this.handleChange}
              value={this.state.goal}
            />
          </FormGroup>

          <FormGroup controlId="content">
            <ControlLabel>Kirjeldus</ControlLabel>
            <FormControl
              componentClass="textarea"
              name="content"
              placeholder="Kirjeldus"
              onChange={this.handleChange}
              value={this.state.content}
            />
          </FormGroup>

          <button
            type="submit"
            disabled={!this.isFormvalid()}
          >
            Salvesta
          </button>
        </form>

        {requestSuccess &&
          <span className="add-clients-success"> Kohtumise lisamine õnnestus! </span>
        }
        {requestError &&
          <span className="add-clients-success"> Kohtumise lisamine ebaõnnestus! </span>
        }
      </div>
    );
  }
}

const mapState = state => ({
  requestSuccess: meetsSuccessSelector(state),
  requestError: meetsErrorSelector(state),
});

const mapDispatch = dispatch => ({
  addMeet: data => dispatch(addMeet(data)),
  emptySuccessError: () => dispatch(emptySuccessError()),
});

export default connect(mapState, mapDispatch)(AddMeet);
