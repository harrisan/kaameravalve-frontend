// @flow

import React from 'react';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { getMeets } from '../actions';
import { meetsListSelector } from '../selectors';

require('../styles/MeetsList.scss');

type Props = {
  meets: Array<Object>;
  getMeets: () => void;
};

class MeetsList extends React.Component {
  props: Props;

  constructor(props: Props) {
    super(props);
    this.state = { search: '' };
  }

  componentWillMount() {
    this.props.getMeets();
  }

  handleSearch = ({ target: { value } }: Object) => {
   // this.props.search(value || '');
  }

  render() {
    const { meets } = this.props;
console.log(meets);
    return (
      <div className="meets-list list-table">
        <h2>Klientidega kohtumised</h2>
        <input type="text" value={this.state.search} onChange={this.handleSearch} />
        <table>
          <thead>
            <th>Kuupäev</th>
            <th>Kliendi nimi</th>
            <th>Eesmärk</th>
            <th>Sisu</th>
          </thead>
          <tbody>
            {meets ? meets.map(meet => (
              <tr>
                <td>
                  {moment().format('llll')}
                </td>
                <td>
                  {meet.clientName}
                </td>
                <td>
                  {meet.goal}
                </td>
                <td>
                  {meet.content}
                </td>
              </tr>
            ))
            :
              <div>Kohtumisi ei leitud</div>
            }
          </tbody>
        </table>
        <button>
          <Link to="/products/add">Lisa kohtumine</Link>
        </button>
      </div>
    );
  }
}

const mapState = state => ({ meets: meetsListSelector(state) });

const mapDispatch = dispatch => ({
  navigate: url => dispatch(push(url)),
  getMeets: () => dispatch(getMeets()),
 // deleteProduct: id => dispatch(deleteProduct(id)),
});

export default connect(mapState, mapDispatch)(MeetsList);

