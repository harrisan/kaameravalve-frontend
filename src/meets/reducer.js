// @flow

import persistConstants from 'redux-persist/constants';
import * as c from './constants';
import * as t from './actionTypes';
import type { State } from './model';

const initialState: State = {
  meets: [],
  meetsLoading: false,
  meetsError: null,
  meetsSuccess: false,
  meetsSearch: '',
};

export default function reducer(state: State = initialState, action: Object = {}): State {
  switch (action.type) {
    case persistConstants.REHYDRATE: {
      const incoming = action.payload[c.NAME];
      if (incoming) {
        incoming.meetsSuccess = false;
        incoming.meetsError = null;
        incoming.meetsLoading = false;
      }

      return Object.assign({}, initialState, state, incoming);
    }

    case t.GET_MEETS_REQUEST: {
      return {
        ...state,
        meetsLoading: true,
      };
    }

    case t.GET_MEETS_SUCCESS: {
      let { meets } = state;
      if (action.payload && action.payload.meets) {
        meets = action.payload.meets;
      }

      return {
        ...state,
        meetsError: null,
        meetsSuccess: false,
        meetsLoading: false,
        meets,
      };
    }

    case t.GET_MEETS_FAILURE: {
      return {
        ...state,
        meetsError: 'Midagi läks valesti, proovi värskendada',
        meetsSuccess: false,
        meetsLoading: false,
      };
    }

    case t.ADD_MEET_REQUEST: {
      return {
        ...state,
        meetsLoading: true,
        meetsSuccess: false,
      };
    }

    case t.ADD_MEET_SUCCESS: {
      console.log('wut');
      return {
        ...state,
        meetsSuccess: true,
        meetsLoading: false,
        meetsError: null,
      };
    }

    case t.ADD_MEET_FAILURE: {
      return {
        ...state,
        meetsError: 'Midagi läks valesti, proovi värskendada',
        meetsSuccess: false,
        meetsLoading: false,
      };
    }

    case t.EMPTY_SUCCESS_ERROR: {
      return {
        ...state,
        meetsError: null,
        meetsSuccess: false,
        meetsLoading: false,
      };
    }


    default: {
      return state;
    }
  }
}
