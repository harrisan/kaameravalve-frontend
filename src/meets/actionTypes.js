import * as t from './constants';

export const GET_MEETS_REQUEST = `${t}/GET_MEETS_REQUEST`;
export const GET_MEETS_SUCCESS = `${t}/GET_MEETS_SUCCESS`;
export const GET_MEETS_FAILURE = `${t}/GET_MEETS_FAILURE`;

export const ADD_MEET_REQUEST = `${t}/ADD_MEET_REQUEST`;
export const ADD_MEET_SUCCESS = `${t}/ADD_MEET_SUCCESS`;
export const ADD_MEET_FAILURE = `${t}/ADD_MEET_FAILURE`;

export const EMPTY_SUCCESS_ERROR = `${t}/EMPTY_SUCCESS_ERROR`;