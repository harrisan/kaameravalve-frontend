import 'whatwg-fetch';
import qs from 'qs';

import { HTTPError, APIError } from './errors';

export const toJson = response => response.json();

export const checkResponse = response => {
  if (!response.ok) {
    throw new HTTPError(response.status, response.statusText);
  }
  return response;
};

export const checkAPIResponse = response => {
  if (!response.status) {
    if (response.response) {
      const { errorInfo, errorCode } = response.response;
      throw new APIError(errorCode, errorInfo);
    }
    throw new Error('Invalid response');
  }
  return response;
};

export const unWrapAPIResponse = response => response.response;

export const apiFetch = (...args) => (
  fetch.apply(undefined, args)
    .then(checkResponse)
    .then(toJson)
    .then(checkAPIResponse)
    .then(unWrapAPIResponse)
);

export const get = (url, params = {}) => (
  apiFetch(`${url}?${qs.stringify(params)}`, {
    method: 'GET',
    headers: {},
    credentials: 'include',
  })
);

export const post = (url, body) => (
  apiFetch(url, {
    method: 'POST',
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    credentials: 'include',
    body: qs.stringify(body),
  })
);

const operation = {
  initWith: 'session',
  format: 'json',
  usageMethod: 'Web',
  version: '2.7',
};

export const postOperation = params => post(window.BACKEND_URL, { ...operation, ...params });
