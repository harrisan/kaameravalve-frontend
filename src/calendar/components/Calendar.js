// @flow

import React from 'react';

require('../styles/Calendar.scss');

const Calendar = () => (
  <div className="calendar-view">
    <iframe
      src="https://teamup.com/ks09175eafc7c69bf8"
      title="TeamUp calendar"
      frameBorder="0"
      width="100%"
      height="700"
    />
  </div>
);

export default Calendar;
