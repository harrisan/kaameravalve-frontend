// @flow
import { CALL_API } from 'redux-api-middleware';
import * as t from './actionTypes';

export const getProducts = () => ({
  [CALL_API]: {
    endpoint: `${API_URL}/products`,
    method: 'GET',
    headers: {},
    types: [
      t.GET_PRODUCTS_REQUEST,
      t.GET_PRODUCTS_SUCCESS,
      t.GET_PRODUCTS_FAILURE,
    ],
  },
});

export const addProduct = (product: Object) => ({
  [CALL_API]: {
    endpoint: `${API_URL}/products`,
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(product),
    types: [
      t.ADD_PRODUCT_REQUEST,
      t.ADD_PRODUCT_SUCCESS,
      t.ADD_PRODUCT_FAILURE,
    ],
  },
});

export const deleteProduct = (id: number) => ({
  [CALL_API]: {
    endpoint: `${API_URL}/products/${id}`,
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json' },
    types: [
      t.DELETE_PRODUCT_REQUEST,
      t.DELETE_PRODUCT_SUCCESS,
      t.DELETE_PRODUCT_FAILURE,
    ],
  },
});

export const emptySuccess = () => ({
  type: t.EMPTY_SUCCESS,
});

export const productSearch = (searchString: string) => ({
  type: t.SEARCH_PRODUCT,
  payload: { searchString },
});
