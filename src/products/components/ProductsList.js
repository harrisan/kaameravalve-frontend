// @flow

import React from 'react';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { getProducts, deleteProduct, productSearch } from '../actions';
import { productsListWithSearchSelector, productsSearchTermSelector } from '../selectors';

type Props = {
  productsList: Array<Object>;
  deleteProduct: (id: string) => void;
  getProducts: () => void;
  navigate: (n: string) => void;
};

class ProductsList extends React.Component {
  props: Props;

  componentWillMount() {
    this.props.getProducts();
  }

  componentWillUnmount() {
    this.props.search('');
  }

  handleSearch = ({ target: { value } }: Object) => {
    this.props.search(value || '');
  }

  handleDelete = (id: string) => {
    const confirmation = confirm('Oled kindel, et soovid toote kustutada?');
    if (confirmation) {
      this.props.deleteProduct(id);
    }
  }

  render() {
    const { productsList, searchTerm } = this.props;

    return (
      <div className="products-list list-table">
        <h2>Toodete nimekiri</h2>
        <input type="text" value={searchTerm} onChange={this.handleSearch} />
        <table>
          <thead>
            <th>
              Kood
            </th>
            <th>
              Nimi
            </th>
            <th>
              Tüki hind
            </th>
            <th>
              Saadavus
            </th>
          </thead>
          <tbody>
            {productsList.map(product => (
              <tr>
                <td>
                  {product.code}
                </td>
                <td>
                  {product.name}
                </td>
                <td>
                  {product.price}
                </td>
                <td>
                  {product.available}
                </td>
                <td
                  className="delete-btn"
                  onClick={() => this.handleDelete(product.id)}
                >
                  Kustuta
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        <button>
          <Link to="/products/add">Lisa uus toode</Link>
        </button>
      </div>
    );
  }
}

const mapState = state => ({
  productsList: productsListWithSearchSelector(state),
  searchTerm: productsSearchTermSelector(state),
});

const mapDispatch = dispatch => ({
  navigate: url => dispatch(push(url)),
  getProducts: () => dispatch(getProducts()),
  deleteProduct: id => dispatch(deleteProduct(id)),
  search: str => dispatch(productSearch(str)),
});

export default connect(mapState, mapDispatch)(ProductsList);

