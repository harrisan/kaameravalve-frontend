// @flow

import React from 'react';

require('../styles/ProductDetails.scss');

type Props = {
  product: Object;
};

const ProductDetails = (props: Props) => {
  const { name, code, description, id, available, price } = props.product;
  return (
    <div className="product-details">
      <div>
        <p>ID: <span>{id}</span></p>
        <p>Nimi: <span>{name}</span></p>
        <p>Saadaval: <span>{available && 'Jah'}</span></p>
      </div>
      <div>
        <p>Tootekood: <span>{code}</span></p>
        <p>Tüki hind: <span>{price}</span></p>
        <p>
          Kirjeldus: <span>{description}</span>
        </p>
      </div>
    </div>
  );
};

export default ProductDetails;
