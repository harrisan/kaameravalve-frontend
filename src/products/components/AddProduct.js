// @flow

import React from 'react';
import { connect } from 'react-redux';
import { FormGroup, ControlLabel, FormControl, Checkbox } from 'react-bootstrap';
import { addProduct, emptySuccess } from '../actions';
import { productsSuccessSelector, productsErrorSelector } from '../selectors';
import {emailValid, notEmpty, phoneValid} from '../../utils/validators';

require('../styles/Products.scss');

const initialState = {
  code: '',
  name: '',
  price: '',
  description: '',
  available: true,
};

type Props = {
  requestSuccess: boolean;
  addProduct: (p: Object) => void;
  emptySuccess: () => void;
  requestError: ?string;
}

type State = {
  code: ?string;
  name: ?string;
  price: ?string;
  description: ?string;
  available: boolean;
}

class AddProduct extends React.Component {
  props: Props;
  state: State;
  handleSubmit: () => void;
  handleChange: (e: Object) => void;
  handleCheckboxChange: () => void;
  getValidationState: (id: String) => void;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    this.productForm = null;
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.requestSuccess) {
      this.setState({ ...initialState });
      this.productForm.reset();
    }
  }

  getValidationState = (controlId: string) => {
    const field = this.state[controlId];
    if (field === null) {
      return null;
    }

    return field && field !== '' ? null : 'error';
  }

  handleSubmit = (e: Event) => {
    e.preventDefault();
    this.props.addProduct(this.state);
  }

  handleChange = (e: Event) => {
    if (this.props.requestSuccess) {
      this.props.emptySuccess();
    }

    this.setState({ [e.target.id]: e.target.value });
  }

  handleCheckboxChange = () => {
    this.setState({ available: !this.state.available });
  }

  isFormvalid = () => {
    const { name, code, price  } = this.state;
    return (
      notEmpty(name)
      && notEmpty(code)
      && notEmpty(price)
    );
  };

  render() {
    const { requestSuccess, requestError } = this.props;

    return (
      <div className="add-product">
        <h2>Sisesta uus toode</h2>
        <form
          className="add-product-form"
          id="addProductForm"
          action=""
          autoComplete="off"
          onSubmit={this.handleSubmit}
          ref={el => this.productForm = el}
        >
          <FormGroup controlId="name" validationState={this.getValidationState('name')}>
            <ControlLabel>Toote nimi</ControlLabel>
            <FormControl
              type="text"
              name="name"
              placeholder="Toote nimi"
              onChange={this.handleChange}
              value={this.state.name}
            />
          </FormGroup>

          <FormGroup controlId="code" validationState={this.getValidationState('code')}>
            <ControlLabel>Toote kood</ControlLabel>
            <FormControl
              type="text"
              name="code"
              placeholder="Kood"
              onChange={this.handleChange}
              value={this.state.code}
            />
          </FormGroup>

          <FormGroup controlId="price" validationState={this.getValidationState('price')}>
            <ControlLabel>Tüki hind</ControlLabel>
            <FormControl
              type="number"
              name="price"
              placeholder="Price"
              onChange={this.handleChange}
              value={this.state.price}
            />
          </FormGroup>

          <FormGroup controlId="available" >
            <Checkbox
              checked={this.state.available}
              onChange={this.handleCheckboxChange}
            >
              Saadaval
            </Checkbox>
          </FormGroup>

          <FormGroup controlId="description" validationState={this.getValidationState('description')}>
            <ControlLabel>Kirjeldus</ControlLabel>
            <FormControl
              componentClass="textarea"
              name="description"
              placeholder="Kirjeldus"
              onChange={this.handleChange}
              value={this.state.description}
            />
          </FormGroup>

          <button
            type="submit"
            disabled={!this.isFormvalid()}
          >
            Salvesta
          </button>
        </form>

        {requestSuccess &&
          <span className="add-clients-success"> Toote lisamine õnnestus! </span>
        }
        {requestError &&
          <span className="add-clients-success"> Toote lisamine ebaõnnestus! </span>
        }
      </div>
    );
  }
}

const mapState = state => ({
  requestSuccess: productsSuccessSelector(state),
  requestError: productsErrorSelector(state),
});

const mapDispatch = dispatch => ({
  addProduct: client => dispatch(addProduct(client)),
  emptySuccess: () => dispatch(emptySuccess()),
});

export default connect(mapState, mapDispatch)(AddProduct);
