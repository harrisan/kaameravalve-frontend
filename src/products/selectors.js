import { createSelector } from 'reselect';
import { NAME } from './constants';
import { includes } from '../utils/functions';

const productsSelector = state => state[NAME];

export const productsListSelector = createSelector(
  productsSelector,
  productsState => productsState.productsList,
);

export const productsSearchTermSelector = createSelector(
  productsSelector,
  productsState => productsState.productsSearch,
);

export const productsLoadingSelector = createSelector(
  productsSelector,
  productsState => productsState.productsLoading,
);

export const productsSuccessSelector = createSelector(
  productsSelector,
  productsState => productsState.productsSuccess,
);

export const productsErrorSelector = createSelector(
  productsSelector,
  productsState => productsState.productsError,
);

export const productsListWithSearchSelector = createSelector(
  productsListSelector,
  productsSearchTermSelector,
  (productList, search) => {
    if (!productList.length) return [];
    if (!search) return productList;

    return productList.filter(p => (
      includes(p.code, search) ||
      includes(p.name, search)
    ));
  },
);
