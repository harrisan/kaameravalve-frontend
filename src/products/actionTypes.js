import * as t from './constants';

export const GET_PRODUCTS_REQUEST = `${t}/GET_PRODUCTS_REQUEST`;
export const GET_PRODUCTS_SUCCESS = `${t}/GET_PRODUCTS_SUCCESS`;
export const GET_PRODUCTS_FAILURE = `${t}/GET_PRODUCTS_FAILURE`;

export const ADD_PRODUCT_REQUEST = `${t}/ADD_PRODUCT_REQUEST`;
export const ADD_PRODUCT_SUCCESS = `${t}/ADD_PRODUCT_SUCCESS`;
export const ADD_PRODUCT_FAILURE = `${t}/ADD_PRODUCT_FAILURE`;

export const DELETE_PRODUCT_REQUEST = `${t}/DELETE_PRODUCT_REQUEST`;
export const DELETE_PRODUCT_SUCCESS = `${t}/DELETE_PRODUCT_SUCCESS`;
export const DELETE_PRODUCT_FAILURE = `${t}/DELETE_PRODUCT_FAILURE`;

export const EMPTY_SUCCESS = `${t}/EMPTY_SUCCESS`;

export const SEARCH_PRODUCT = `${t}/SEARCH_PRODUCT`;

