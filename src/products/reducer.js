// @flow

import persistConstants from 'redux-persist/constants';
import * as c from './constants';
import * as t from './actionTypes';
import type { State } from './model';

const initialState: State = {
  productsList: [],
  productsLoading: false,
  productsError: null,
  productsSuccess: false,
  productsSearch: '',
};

export default function reducer(state: State = initialState, action: Object = {}): State {
  switch (action.type) {
    case persistConstants.REHYDRATE: {
      const incoming = action.payload[c.NAME];
      if (incoming) {
        incoming.productsSuccess = false;
        incoming.productsError = null;
        incoming.productsLoading = false;
        incoming.productsSearch = '';
      }

      return Object.assign({}, initialState, state, incoming);
    }

    case t.GET_PRODUCTS_REQUEST: {
      return {
        ...state,
        productsLoading: true,
      };
    }

    case t.GET_PRODUCTS_SUCCESS: {
      let { productsList } = state;
      if (action.payload && action.payload.products) {
        productsList = action.payload.products;
      }

      return {
        ...state,
        productsError: null,
        productsSuccess: false,
        productsLoading: false,
        productsList,
      };
    }

    case t.GET_PRODUCTS_FAILURE: {
      return {
        ...state,
        productsError: 'Midagi läks valesti, proovi värskendada',
        productsSuccess: false,
        productsLoading: false,
      };
    }

    case t.ADD_PRODUCT_REQUEST: {
      return {
        ...state,
        productsLoading: true,
        productsSuccess: false,
      };
    }

    case t.ADD_PRODUCT_SUCCESS: {
      const productsList = [...state.productsList];
      const productId = productsList.length + 1;
      const newProduct = {
        ...action.payload,
        productId,
      };

      productsList.push(newProduct);
      return {
        ...state,
        productsLoading: false,
        productsSuccess: true,
        productsList,
      };
    }

    case t.ADD_PRODUCT_FAILURE: {
      return {
        ...state,
        productsLoading: false,
        productSuccess: false,
        productsError: 'Something went wrong',
      };
    }

    case t.DELETE_PRODUCT_REQUEST: {
      return {
        ...state,
        productsError: null,
      };
    }

    case t.DELETE_PRODUCT_SUCCESS: {
      let { productsList } = state;
      if (action.payload && action.payload.product) {
        productsList = productsList.filter(p => p.id !== parseInt(action.payload.product.id, 10));
      }

      return {
        ...state,
        productsList,
        productsError: null,
      };
    }

    case t.DELETE_PRODUCT_FAILURE: {
      return {
        ...state,
        productsError: 'Toote eemaldamine ebaõnnestus',
      };
    }

    case t.EMPTY_SUCCESS: {
      return {
        ...state,
        productsLoading: false,
        productSuccess: false,
        productsError: null,
      };
    }

    case t.SEARCH_PRODUCT: {
      return {
        ...state,
        productsSearch: action.payload.searchString,
      };
    }

    default: {
      return state;
    }
  }
}
