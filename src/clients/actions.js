import { CALL_API } from 'redux-api-middleware';
import * as t from './actionTypes';

export const setSuccessDefault = () => ({
  type: t.SET_SUCCESS_DEFAULT,
});

export const clientSearch = (searchString: string) => ({
  type: t.SEARCH_CLIENT,
  payload: { searchString },
});

export function getClients() {
  return {
    [CALL_API]: {
      endpoint: `${API_URL}/clients`,
      method: 'GET',
      headers: {},
      types: [
        t.GET_CLIENTS_REQUEST,
        t.GET_CLIENTS_SUCCESS,
        t.GET_CLIENTS_FAILURE,
      ],
    },
  };
}

export function addClient(client: Object) {
  return {
    [CALL_API]: {
      endpoint: `${API_URL}/clients`,
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(client),
      types: [
        t.ADD_CLIENT_REQUEST,
        t.ADD_CLIENT_SUCCESS,
        t.ADD_CLIENT_FAILURE,
      ],
    },
  };
}

export function deleteClient(id: number) {
  return {
    [CALL_API]: {
      endpoint: `${API_URL}/clients/${id}`,
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json' },
      types: [
        t.DELETE_CLIENT_REQUEST,
        t.DELETE_CLIENT_SUCCESS,
        t.DELETE_CLIENT_FAILURE,
      ],
    },
  };
}
