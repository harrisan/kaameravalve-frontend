import * as t from './constants';

export const GET_CLIENTS_REQUEST = `${t}/GET_CLIENTS_REQUEST`;
export const GET_CLIENTS_SUCCESS = `${t}/GET_CLIENTS_SUCCESS`;
export const GET_CLIENTS_FAILURE = `${t}/GET_CLIENTS_FAILURE`;

export const ADD_CLIENT_REQUEST = `${t}/ADD_CLIENT_REQUEST`;
export const ADD_CLIENT_SUCCESS = `${t}/ADD_CLIENT_SUCCESS`;
export const ADD_CLIENT_FAILURE = `${t}/ADD_CLIENT_FAILURE`;

export const DELETE_CLIENT_REQUEST = `${t}/DELETE_CLIENT_REQUEST`;
export const DELETE_CLIENT_SUCCESS = `${t}/DELETE_CLIENT_SUCCESS`;
export const DELETE_CLIENT_FAILURE = `${t}/DELETE_CLIENT_FAILURE`;


export const SET_SUCCESS_DEFAULT = `${t}/SET_SUCCESS_DEFAULT`;

export const SEARCH_CLIENT = `${t}/SEARCH_CLIENT`;

