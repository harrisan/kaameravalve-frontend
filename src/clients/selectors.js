import { createSelector } from 'reselect';
import { NAME } from './constants';
import { includes } from '../utils/functions';

const clientsSelector = state => state[NAME];

export const clientsListSelector = createSelector(
  clientsSelector,
  clientsState => clientsState.clientsList,
);

export const clientsSearchTermSelector = createSelector(
  clientsSelector,
  clientsState => clientsState.clientSearch,
);

export const clientsLoadingSelector = createSelector(
  clientsSelector,
  clientsState => clientsState.clientsLoading,
);

export const clientsSuccessSelector = createSelector(
  clientsSelector,
  clientsState => clientsState.clientsSuccess,
);

export const clientsErrorSelector = createSelector(
  clientsSelector,
  clientsState => clientsState.clientsError,
);


export const clientListWithSearchSelector = createSelector(
  clientsListSelector,
  clientsSearchTermSelector,
  (clientList, search) => {
    if (!clientList.length) return [];
    if (!search) return clientList;

    return clientList.filter(c => (
      includes(c.name, search) ||
      includes(c.phone, search) ||
      includes(c.address, search) ||
      includes(c.company, search) ||
      includes(c.email, search)
    ));
  },
);


export const searchReports = (state: State, filter: ?string) => {
  const clients = clientsListSelector(state);
  if (!clients.length) return [];
  if (!filter) return clients;

  return clients.filter(v => (
    includes(v.name, filter) ||
    includes(v.phone, filter) ||
    includes(v.address, filter) ||
    includes(v.company, filter) ||
    includes(v.email, filter)
  ));
};