// @flow

import persistConstants from 'redux-persist/constants';
import * as c from './constants';
import * as t from './actionTypes';
import type { State } from './model';

const initialState: State = {
  clientsList: [],
  clientsLoading: false,
  clientsError: null,
  clientsSuccess: false,
  clientSearch: '',
};

export default function reducer(state: State = initialState, action: Object = {}): State {
  switch (action.type) {
    case persistConstants.REHYDRATE: {
      const incoming = action.payload[c.NAME];
      if (incoming) {
        incoming.clientsSuccess = false;
        incoming.clientError = null;
        incoming.clientsLoading = false;
      }

      return Object.assign({}, initialState, state, incoming);
    }

    case t.GET_CLIENTS_REQUEST: {
      return {
        ...state,
        clientsLoading: true,
        clientsError: null,
      };
    }

    case t.GET_CLIENTS_SUCCESS: {
      let { clientsList } = state;
      if (action.payload && action.payload.clients) {
        clientsList = action.payload.clients;
      }

      return {
        ...state,
        clientsError: null,
        clientsSuccess: false,
        clientsLoading: false,
        clientsList,
      };
    }

    case t.GET_CLIENTS_FAILURE: {
      return {
        ...state,
        clientsError: 'Midagi läks valesti, proovi värskendada',
        clientsSuccess: false,
        clientsLoading: false,
      };
    }

    case t.ADD_CLIENT_REQUEST: {
      return {
        ...state,
        clientsLoading: true,
        clientsSuccess: false,
        clientsError: null,
      };
    }

    case t.ADD_CLIENT_SUCCESS: {
      const clientsList = [...state.clientsList];
      if (action.payload.client) {
        clientsList.push(action.payload.client);
      }

      return {
        ...state,
        clientsLoading: false,
        clientsSuccess: true,
        clientsError: null,
        clientsList,
      };
    }

    case t.ADD_CLIENT_FAILURE: {
      return {
        ...state,
        clientsLoading: false,
        clientsSuccess: false,
        clientsError: 'Midagi läks valesti, kontrolli väljasid ja proovi uuesti',
      };
    }


    case t.DELETE_CLIENT_SUCCESS: {
      let { clientsList } = state;
      if (action.payload && action.payload.client) {
        clientsList = clientsList.filter(c => c.id !== parseInt(action.payload.client.id, 10));
      }

      return {
        ...state,
        clientsList,
        clientsError: null,
      };
    }

    case t.DELETE_CLIENT_FAILURE: {
      return {
        ...state,
        clientsError: action.payload.response.message,
      };
    }

    case t.SET_SUCCESS_DEFAULT: {
      return {
        ...state,
        clientsLoading: false,
        clientsSuccess: false,
        clientsError: null,
      };
    }

    case t.SEARCH_CLIENT: {
      return {
        ...state,
        clientSearch: action.payload.searchString,
        clientsError: null,
      };
    }

    default: {
      return state;
    }
  }
}
