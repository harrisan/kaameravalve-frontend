// @flow

import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { clientSearch, getClients, deleteClient } from '../actions';
import { clientListWithSearchSelector, clientsErrorSelector, clientsSearchTermSelector } from '../selectors';

require('../styles/ClientsList.scss');

type Props = {
  clientsList: Array<Object>;
  searchTerm: string;
  search: (s: string) => void;
  getClients: () => void;
  deleteClient: (c: string) => void;
}

class ClientsList extends React.Component {
  props: Props;

  componentWillMount() {
    this.props.getClients();
  }

  handleSearch = ({ target: { value } }: Object) => {
    this.props.search(value || '');
  }

  handleDelete = (id: string) => {
    const confirmation = confirm('Oled kindel, et soovid toote kustutada?');
    if (confirmation) {
      this.props.deleteClient(id);
    }
  }

  render() {
    const { clientsList, searchTerm, requestError } = this.props;

    return (
      <div className="clients-list list-table">
        <h2>Klientide nimekiri</h2>
        <input type="text" value={searchTerm} onChange={this.handleSearch} />
        <table>
          <thead>
            <th>
              Nimi
            </th>
            <th>
              Firma
            </th>
            <th>
              Email
            </th>
            <th>
              Telefon
            </th>
            <th>
              Aadress
            </th>
          </thead>
          <tbody>
            {clientsList.map((client, key) => (
              <tr key={key}>
                <td>
                  {client.name}
                </td>
                <td>
                  {client.companyName}
                </td>
                <td>
                  {client.email}
                </td>
                <td>
                  {client.phone}
                </td>
                <td>
                  {client.address}
                </td>
                <td
                  className="delete-btn"
                  onClick={() => this.handleDelete(client.id)}
                >
                  Kustuta
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        {requestError &&
          <span className="add-clients-success">{requestError}</span>
        }
        <button>
          <Link to="/clients/add">Lisa uus klient</Link>
        </button>
      </div>
    );
  }
}

const mapState = state => ({
  clientsList: clientListWithSearchSelector(state),
  searchTerm: clientsSearchTermSelector(state),
  requestError: clientsErrorSelector(state),
});

const mapDispatch = dispatch => ({
  search: term => dispatch(clientSearch(term)),
  getClients: () => dispatch(getClients()),
  deleteClient: id => dispatch(deleteClient(id)),
});

export default connect(mapState, mapDispatch)(ClientsList);

