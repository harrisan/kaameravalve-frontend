// @flow

import React from 'react';
import { connect } from 'react-redux';
import { FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import { addClient, setSuccessDefault } from '../actions';
import { clientsSuccessSelector, clientsErrorSelector } from '../selectors';
import { notEmpty, emailValid, phoneValid } from '../../utils/validators';

require('../styles/AddClients.scss');

const initialState = {
  name: null,
  phone: null,
  address: null,
  email: null,
  companyName: null,
};

type Props = {
  onSave: () => void;
  requestSuccess: boolean;
  requestError: boolean;
  addClient: (client: Object) => void;
  setSuccessDefault: () => void;
}

type State = {
  name: ?string;
  phone: ?string;
  address: ?string;
  email: ?string;
  companyName: ?string;
}

class AddClients extends React.Component {
  props: Props;
  state: State;

  constructor(props) {
    super(props);

    this.state = initialState;
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.requestSuccess) {
      this.setState(initialState);
    }
  }

  getValidationState = (controlId: string) => {
    const field = this.state[controlId];
    if (field === null) {
      return null;
    }

    if (controlId === 'email') {
      return emailValid(this.state[controlId]) ? null : 'error';
    }

    if (controlId === 'phone') {
      return phoneValid(this.state[controlId]) ? null : 'error';
    }

    return field && field !== '' ? null : 'error';
  }

  handleSubmit = (e: Event) => {
    e.preventDefault();
    if (this.isFormValid()) {
      this.props.addClient(this.state);

      if (this.props.onSave) {
        this.props.onSave();
      }
    }
  }

  handleChange = (e: Event) => {
    if (this.props.requestSuccess || this.props.requestError) {
      this.props.setSuccessDefault();
    }

    this.setState({ [e.target.id]: e.target.value });
  }

  isFormValid = () => {
    const { name, phone, address, email } = this.state;
    return (
      notEmpty(name)
      && notEmpty(address)
      && phoneValid(phone)
      && emailValid(email)
    );
  }

  render() {
    const { requestSuccess, requestError } = this.props;
    const formValid = this.isFormValid();
    return (
      <div className="add-clients">
        <h2>Sisesta uus klient</h2>
        <form
          className="add-clients-form"
          id="add-client-form"
          action=""
          autoComplete="off"
          onSubmit={this.handleSubmit}
        >
          <FormGroup controlId="name" validationState={this.getValidationState('name')}>
            <ControlLabel>Kliendi nimi</ControlLabel>
            <FormControl
              type="text"
              name="name"
              placeholder="Kliendi nimi"
              onChange={this.handleChange}
              value={this.state.name}
            />
          </FormGroup>

          <FormGroup controlId="phone" validationState={this.getValidationState('phone')}>
            <ControlLabel>Telefon</ControlLabel>
            <FormControl
              type="text"
              name="phone"
              placeholder="Telefon"
              onChange={this.handleChange}
              value={this.state.phone}
            />
          </FormGroup>

          <FormGroup controlId="address" validationState={this.getValidationState('address')}>
            <ControlLabel>Aadress</ControlLabel>
            <FormControl
              type="text"
              name="address"
              placeholder="Aadress"
              onChange={this.handleChange}
              value={this.state.address}
            />
          </FormGroup>

          <FormGroup controlId="email" validationState={this.getValidationState('email')}>
            <ControlLabel>Email</ControlLabel>
            <FormControl
              type="text"
              name="email"
              placeholder="Email"
              onChange={this.handleChange}
              value={this.state.email}
            />
          </FormGroup>

          <FormGroup controlId="companyName">
            <ControlLabel>Firma</ControlLabel>
            <FormControl
              type="text"
              name="companyName"
              placeholder="Firma nimi"
              onChange={this.handleChange}
              value={this.state.company}
            />
          </FormGroup>

          <button type="submit" disabled={!formValid}>Salvesta</button>
        </form>

        {requestSuccess &&
          <span className="add-clients-success"> Kliendi lisamine õnnestus! </span>
        }
        {requestError &&
        <span className="add-clients-success">Kliendi lisamine ebaõnnestus, palun proovi uuesti</span>
        }
      </div>
    );
  }
}

const mapState = state => ({
  requestSuccess: clientsSuccessSelector(state),
  requestError: clientsErrorSelector(state),
});

const mapDispatch = dispatch => ({
  addClient: client => dispatch(addClient(client)),
  setSuccessDefault: () => dispatch(setSuccessDefault()),
});

export default connect(mapState, mapDispatch)(AddClients);