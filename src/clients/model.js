// @flow

export type State = {
  clientsList: [
    {
      name: ?string;
      email: ?string;
      phone: ?string;
      address: ?string;
      company: ?string;
    },
  ],
  clientsLoading: boolean,
  clientError: ?string;
  clientSuccess: boolean,
};
