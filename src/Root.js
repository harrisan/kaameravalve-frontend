// @flow

import React from 'react';
import { Route } from 'react-router';
import { BrowserRouter, Switch, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
import { compose, createStore, applyMiddleware } from 'redux';
import { persistStore, autoRehydrate } from 'redux-persist';
import { apiMiddleware } from 'redux-api-middleware';
import { routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import reducer from './core/rootReducer';
import products from './products';
import offers from './offers';
import clients from './clients';
import calendar from './calendar';
import session from './session';
import meets from './meets';
import Header from './header/components/Header';
import { sessionExpiredMiddleware, requestMiddleware } from './core/middlewares';

require('./core/styles/index.scss');

const history = createHistory();

const store = createStore(
  reducer,
  compose(
    autoRehydrate(),
    sessionExpiredMiddleware, // ?!
    applyMiddleware(
      requestMiddleware,
      apiMiddleware,
      routerMiddleware(history),
    ),
  ),
);

function isAuthenticated(path = '/') {
  const state = store.getState();
  const authToken = state[session.constants.NAME].authToken;
  return authToken !== null;
}

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
    isAuthenticated() === true
      ? <Component {...props} />
      : <Redirect to='/login' />
  )}
  />
)

class Root extends React.Component {
  state: State;
  routeFilters: () => void;

  constructor(props: any) {
    super(props);

    this.state = {
      rehydrated: false,
    };
  }

  /**
   * This will be run once when the app starts.
   *
   * First we set up state persisting via localForage and check if
   * we have any previously persisted state.
   * Then if we find an existing session token, check its validity.
   * Finally let the component know, that we're done with prep.
   */
  componentWillMount() {
    const blacklist = [
      'CORE',
    ];

    persistStore(store, { blacklist }, () => {
      this.setState({ rehydrated: true });
    });
  }

  render() {
    if (!this.state.rehydrated) {
      return (null);
    }

    return (
      <Provider store={store}>
        <BrowserRouter>
          <div>
            <Route path="/">
              <div className="app-container">
                {/* Main content area */}
                <Header />
                <div className="wrapper">
                  <div className="container">
                    <Switch >
                      <Route
                        exact
                        path="/login"
                        component={session.components.Login}
                      />
                      <PrivateRoute
                        exact
                        path="/"
                        component={calendar.components.Calendar}
                      />
                      <PrivateRoute
                        exact
                        path="/calendar"
                        component={calendar.components.Calendar}
                      />

                      <PrivateRoute
                        exact
                        path="/clients/add"
                        component={clients.components.AddClients}
                      />
                      <PrivateRoute
                        exact
                        path="/clients/list"
                        component={clients.components.ClientsList}
                      />
                      <PrivateRoute
                        exact
                        path="/offers/list"
                        component={offers.components.OffersList}
                      />
                      <PrivateRoute
                        path="/offers/add"
                        exact
                        component={offers.components.AddOffer}
                      />
                      <PrivateRoute
                        path="/offer/:id"
                        component={offers.components.DetailView}
                      />
                      <PrivateRoute
                        exact
                        path="/products/add"
                        component={products.components.AddProduct}
                      />
                      <PrivateRoute
                        exact
                        path="/products/list"
                        component={products.components.ProductsList}
                      />
                      <PrivateRoute
                        exact
                        path="/meets/list"
                        component={meets.components.MeetsList}
                      />
                      <PrivateRoute
                        exact
                        path="/meets/add"
                        component={meets.components.AddMeet}
                      />
                    </Switch>
                  </div>
                </div>
              </div>
            </Route>
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

type State = {
  rehydrated: boolean;
}

export default Root;
