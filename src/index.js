import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import Root from './Root';

// AppContainer is a wrapper that enables Hot Reloading to work
// In production builds it should just be a pass-through.

const root = document.getElementById('root');

ReactDOM.render(<AppContainer><Root /></AppContainer>, root);

if (module.hot) {
  module.hot.accept('./Root', () => {
    ReactDOM.render(<AppContainer><Root /></AppContainer>, root);
  });
}
