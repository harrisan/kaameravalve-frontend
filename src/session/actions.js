// @flow
import { CALL_API } from 'redux-api-middleware';
import * as t from './actionTypes';

export const login = (username: string, password: string) => {
  const body = JSON.stringify({
    client_id: APPLICATION_ID,
    client_secret: APPLICATION_SECRET,
    grant_type: 'password',
    scope: '*',
    username,
    password,
  });

  return {
    [CALL_API]: {
      endpoint: `${AUTH_API_URL}`,
      body,
      method: 'POST',
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
      types: [
        t.USER_LOGIN_REQUEST,
        t.USER_LOGIN_SUCCESS,
        t.USER_LOGIN_FAILURE,
      ],
    },
  };
};

export const getUserData = () => {
  return {
    [CALL_API]: {
      endpoint: `${API_URL}/user`,
      method: 'GET',
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
      types: [
        t.USER_DATA_REQUEST,
        t.USER_DATA_SUCCESS,
        t.USER_DATA_FAILURE,
      ],
    },
  };
}

export const logout = () => {
  return {
    [CALL_API]: {
      endpoint: `${API_URL}/logout`,
      method: 'GET',
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
      types: [
        t.LOGOUT_REQUEST,
        t.LOGOUT_RESPONSE,
        t.LOGOUT_FAILURE,
      ],
    },
  };
};
