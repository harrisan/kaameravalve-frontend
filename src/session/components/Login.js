// @flow

import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { login, getUserData } from '../actions';
import { authTokenSelector } from '../selectors';

require('../styles/LoginView.scss');

type Props = {
  getUserData: (t: string) => void;
  login: (Object) => void;
  authToken: string;
  history: Object;
}

type State = {
  username: string;
  password: string;
}

class Login extends React.Component {
  props: Props;
  state: State;

  constructor(props: Props) {
    super(props);

    this.state = {
      username: '',
      password: '',
    };
  }

  componentWillMount() {
    window.addEventListener('keydown', (e) => {
      if (e.keyCode === 13) {
        this.login();
      }
    });
  }

  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.authToken && this.props.authToken !== nextProps.authToken) {
      this.props.getUserData(nextProps.authToken);
      nextProps.history.push('/calendar');
    }
  }

  handleFieldChange = (e: Event) => {
    this.setState({ [e.target.id]: e.target.value });
  }

  login = () => {
    this.props.login({ ...this.state });
  };

  render() {
    return (
      <div className="login-view">
        <div className="login-view-inputs">
          <input onChange={this.handleFieldChange} value={this.state.username} placeholder="Kasutaja nimi" id="username" type="text" />
          <input onChange={this.handleFieldChange} value={this.state.password} placeholder="Parool" id="password" type="password" />
          <button onClick={this.login}>Login</button>
        </div>
      </div>
    );
  }
}

const mapState = state => ({ authToken: authTokenSelector(state) });
const mapDispatchToProps = dispatch => ({
  login: ({ username, password }) => dispatch(login(username, password)),
  getUserData: authToken => dispatch(getUserData(authToken)),
});

export default withRouter(connect(mapState, mapDispatchToProps)(Login));
