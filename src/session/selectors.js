import { createSelector } from 'reselect';
import { NAME } from './constants';
import { includes } from '../utils/functions';

const sessionSelector = state => state[NAME];

export const authTokenSelector = createSelector(
  sessionSelector,
  session => session.authToken,
);

export const userDataSelector = createSelector(
  sessionSelector,
  session => session.user || {},
);