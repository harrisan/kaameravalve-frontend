// @flow

import persistConstants from 'redux-persist/constants';
import * as c from './constants';
import * as t from './actionTypes';
import type { State } from './model';

const initialState: State = {
  authToken: null,
  user: {},
};

export default function reducer(state: State = initialState, action: Object = {}): State {
  switch (action.type) {
    case persistConstants.REHYDRATE: {
      const incoming = action.payload[c.NAME];
      let thisState = state;
      if (incoming) {
        thisState = Object.assign({}, incoming, {
          authToken: incoming.authToken,
          user: incoming.user,
        });
      }

      return initialState;
    }

    case t.USER_LOGIN_REQUEST: {
      return {
        ...state,
      };
    }

    case t.USER_LOGIN_SUCCESS: {
      let { authToken } = state;

      if (action.payload.access_token) {
        authToken = action.payload.access_token;
      }

      return {
        ...state,
        authToken,
      };
    }

    case t.USER_LOGIN_FAILURE: {
      return {
        ...state,
      };
    }

    case t.USER_DATA_REQUEST: {
      return {
        ...state,
      };
    }

    case t.USER_DATA_SUCCESS: {
      let { user } = state;
      console.log(action.payload, 'getuserdata');

      if (action.payload) {
        user = action.payload;
      }

      return {
        ...state,
        user,
      };
    }

    case t.USER_DATA_FAILURE: {
      return {
        ...state,
      };
    }

    case t.LOGOUT_RESPONSE: {
      return {
        ...initialState,
      };
    }

    default: {
      return state;
    }
  }
}
