// @flow

import React from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import Header from '../../header/components/Header';
import calendar from '../../calendar';
import session from '../../session';
import clients from '../../clients';
import offers from '../../offers';
import products from '../../products';


type Props = {
  loggedIn: boolean;
  dispatch: (x: ReduxAction) => Promise<any>;
  children: React.Component<*, *, *>;
}

type State = {
  smallScreen: boolean;
}

class Shell extends React.Component {
  props: Props;
  state: State;
  _handleResize: () => void;
  _logout: () => void;

  constructor(...args) {
    super(...args);

    const smallScreen = window.innerWidth <= 768;

    this.state = {
      smallScreen,
    };
  }


  render() {
    const { loggedIn } = this.props;
    const { smallScreen } = this.state;

    return (
      <div className="app-container">
        {/* Main content area */}
        <Header />
        <div className="wrapper">
          <div className="container">
            <Switch>
              <Route
                exact
                path="/login"
                component={session.components.Login}
              />

              <Route
                exact
                path="/"
                component={calendar.components.Calendar}
              />
              <Route
                exact
                path="/calendar"
                component={calendar.components.Calendar}
              />

              <Route
                exact
                path="/clients/add"
                component={clients.components.AddClients}
              />
              <Route
                exact
                path="/clients/list"
                component={clients.components.ClientsList}
              />
              <Route
                exact
                path="/offers/list"
                component={offers.components.OffersList}
              />
              <Route
                path="/offers/add"
                exact
                component={offers.components.AddOffer}
              />
              <Route
                path="/offer/:id"
                component={offers.components.DetailView}
              />
              /*<Route

                path="/offers/:id"
                component={offers.components.DetailView}
              />*/


              /*<Route
                exact
                path="/products"
                component={products.components.ProductsList}
              />*/
              <Route
                exact
                path="/products/add"
                component={products.components.AddProduct}
              />

            </Switch>
          </div>
        </div>
      </div>
    );
  }
}

const mapState = state => ({
});

export default connect(mapState)(Shell);
