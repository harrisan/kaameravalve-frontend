import * as components from './components';
import rootReducer from './rootReducer';
import * as constants from './constants';

/**
 * Core app components
 */
export default { components, rootReducer, constants };
