import { CALL_API } from 'redux-api-middleware';
import cookie from 'react-cookie';
import { SESSION_EXPIRED } from './actionTypes';

// custom middlewares
export const sessionExpiredMiddleware = (next) => (reducer, initialState) => {
  const enhanceReducer = (state, action) => {
    let expired = false;
    let logout = false;
    // FIXME: load elsewhere; this only works in live env (CORS cookie)

    const authToken = state && state.SESSION && state.SESSION.authToken;

    if (action.payload) {
      const { payload } = action;

      if (payload.status === 401 && payload.name === 'ApiError' && payload.statusText === 'Unauthorized') {
        expired = true;
      } else if (payload.status === 304) {
        logout = true;
      }
    }

    // FIXME: let reducers decide what to do when there is no session
    if ((expired || logout)) {
      const newState = {
        CLIENTS: { ...state.CLIENTS },
        OFFERS: { ...state.OFFERS },
        PRODUCTS: { ...state.PRODUCTS },
        SESSION: { user: {}, authToken: null },
        CALENDAR: { ...state.CALENDAR },
        CORE: state.CORE,
      };

      window.location.replace('/');
      return reducer(newState, {});
    }

    return reducer(authToken ? { ...state, ...{ ...state.SESSION, authToken } } : state, action);
  };
  return next(enhanceReducer, initialState);
};

export const requestMiddleware = store => next => action => {
  const callApi = action[CALL_API];

  // Check if this action is a redux-api-middleware action.
  if (callApi) {
    const state = store.getState();
    const { authToken } = state.SESSION;

    if (authToken) {
      action[CALL_API].headers.Authorization = `Bearer ${authToken}`;
    }

    action[CALL_API].headers.Accept = 'application/json';
  }

  // Pass the FSA to the next action.
  return next(action);
}
