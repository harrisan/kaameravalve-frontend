import backgroundImage from '../../../static/background.jpg';

export const HTML_BODY = {
  'html, body': {
    margin: '0',
    padding: '0',
    fontFamily: "'Open Sans', sans-serif",
    backgroundColor: '#f5f6f7',
    height: '100%',
  },
};

export const WRAPPER = {
  height: '100vh',
  backgroundImage: `url(${backgroundImage})`,
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
};

export const CONTENT = {
 // position: 'relative',
  overflowY: 'scroll',
//  maxWidth: '100%',
 // width: '100vw',
 // height: 'inherit',
  transition: 'margin 0.3s ease-out',
  backgroundColor: 'black',
 // marginLeft: 'auto',
 // marginRight: 'auto',
};

export const MENU_WRAPPER = {
  width: '202px',
  overflow: 'hidden',
};


export const NAVIGATION_STYLES = {
  position: 'fixed',
  top: '65px',
  left: '0px',
  zIndex: '2',
  width: '0px',
};

export const HEADER_LOGO = {
  width: '40px',
  height: '40px',
  marginTop: '7px',
  cursor: 'pointer',
};

export const RESPONSIVE_BURGER = {
  cursor: 'pointer',
  paddingTop: '15px',
  display: 'inline-block',
  verticalAlign: 'super',
  position: 'relative',
};

export const BACKDROP = {
  position: 'absolute',
  top: 0,
  height: '100vh',
  zIndex: -1,
  backgroundColor: 'black',
};
/*
export const BACKDROP_SHOWN = {
  ...BACKDROP,
  width: '100vw',
  opacity: '0.5',
  transition: 'opacity 0.3s ease-out',
};

export const BACKDROP_HIDDEN = {
  ...BACKDROP,
  width: '0px',
  opacity: '0',
  transition: 'opacity 0.3s ease-in, width 0s linear 0.3s',
};*/

export const FLEX_CLASSES = {
  mediaQueries: {
    '(max-width: 480px)': {
      'div.col-xxs-12': {
        'flex-basis': '100%',
        'max-width': '100%',
        '-webkit-flex-basis': '100%',
        '-ms-flex-preferred-size': '100%',
      },
      'div.col-xxs-6': {
        'flex-basis': '50%',
        'max-width': '50%',
        '-webkit-flex-basis': '50%',
        '-ms-flex-preferred-size': '50%',
      },
      'div.col-xxs-3': {
        'flex-basis': '25%',
        'max-width': '25%',
        '-webkit-flex-basis': '25%',
        '-ms-flex-preferred-size': '25%',
      },
      'div.col-xxs-4': {
        'flex-basis': '33.333%',
        'max-width': '33.333%',
        '-webkit-flex-basis': '33.333%',
        '-ms-flex-preferred-size': '33.333%',
      },
      '.hide-xxs': {
        display: 'none',
      },
    },
  },
};
