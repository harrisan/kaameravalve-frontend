/**
 * This file includes both the Core module specific reducer and
 * the global reducer combining all module reducers.
 *
 * @flow
 */

import { routerReducer } from 'react-router-redux';
import { combineReducers } from 'redux';
import * as t from './actionTypes';
import { NAME, STORE_VERSION } from './constants';
import session from '../session';
import reports from '../reports';

// CORE module reducer definiton:

type CoreState = {
}

const initialState:CoreState = {
};

function coreReducer(
  state: CoreState = initialState,
  action: ReduxAction
): CoreState {
  switch (action.type) {
    case t.STORE_VERSION_UPDATE:
      return {
        ...state,
        storeVersion: action.payload.storeVersion,
      };

    case t.SEARCH:
      return {
        ...state,
        searchString: action.payload.searchString,
      };

    default:
      return state;
  }
}

export default combineReducers({
  [NAME]: coreReducer,
  routing: routerReducer,
});
