import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import session from '../session';
import clients from '../clients';
import offers from '../offers';
import products from '../products';
import meets from '../meets';


export default combineReducers({
  [session.constants.NAME]: session.reducer,
  [clients.constants.NAME]: clients.reducer,
  [offers.constants.NAME]: offers.reducer,
  [products.constants.NAME]: products.reducer,
  [meets.constants.NAME]: meets.reducer,
  routing: routerReducer,
});
