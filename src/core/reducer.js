// @flow

import { routerReducer } from 'react-router-redux';
import { combineReducers } from 'redux';
import * as t from './actionTypes';
import { NAME } from './constants';

type CoreState = {};

const initialState: CoreState = {};

function coreReducer(
  state: CoreState = initialState,
  action: ReduxAction
): CoreState {
  switch (action.type) {
    default:
      return state;
  }
}

export default combineReducers({
  [NAME]: coreReducer,
  routing: routerReducer,
});
