// @flow
import { CALL_API } from 'redux-api-middleware';
import * as t from './actionTypes';

export const getOffers = () => ({
  [CALL_API]: {
    endpoint: `${API_URL}/offers`,
    method: 'GET',
    headers: {},
    types: [
      t.GET_OFFERS_REQUEST,
      t.GET_OFFERS_SUCCESS,
      t.GET_OFFERS_FAILURE,
    ],
  },
});

export const getOfferDetails = id => ({
  [CALL_API]: {
    endpoint: `${API_URL}/offers/${id}`,
    method: 'GET',
    headers: {},
    types: [
      t.GET_OFFER_DETAILS_REQUEST,
      t.GET_OFFER_DETAILS_SUCCESS,
      t.GET_OFFER_DETAILS_FAILURE,
    ],
  },
});

export const addOffer = (offer: Object) => ({
  [CALL_API]: {
    endpoint: `${API_URL}/offers`,
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(offer),
    types: [
      t.ADD_OFFER_REQUEST,
      t.ADD_OFFER_SUCCESS,
      t.ADD_OFFER_FAILURE,
    ],
  },
});

export const sendOutOffer = (id: number) => ({
  [CALL_API]: {
    endpoint: `${API_URL}/offers/send/${id}`,
    method: 'GET',
    headers: {},
    types: [
      t.SEND_OUT_OFFER_REQUEST,
      t.SEND_OUT_OFFER_SUCCESS,
      t.SEND_OUT_OFFER_FAILURE,
    ],
  },
});

export const clearOffersSuccess = () => ({
  type: t.CLEAR_OFFERS_SUCCESS,
});

