// @flow

import persistConstants from 'redux-persist/constants';
import * as c from './constants';
import * as t from './actionTypes';
import type { State } from './model';

const initialState: State = {
  offers: [],
  offersLoading: false,
  offersSuccess: false,
  detailOffer: {},
};

export default function reducer(state: State = initialState, action: Object = {}): State {
  switch (action.type) {
    case persistConstants.REHYDRATE: {
      return Object.assign({}, initialState, state);
    }

    case t.GET_OFFERS_SUCCESS: {
      return {
        ...state,
        offersLoading: false,
        offersSuccess: false,
        offers: action.payload.offers,
      };
    }

    case t.ADD_OFFER_SUCCESS: {
      const offers = [...state.offers];

      if (action.payload) {
        offers.push(action.payload.offer);
      }
      return {
        ...state,
        offersLoading: false,
        offersSuccess: true,
        offers: [],
      };
    }

    case t.GET_OFFER_DETAILS_SUCCESS: {
      return {
        ...state,
        offersLoading: false,
        offersSuccess: false,
        detailOffer: action.payload.offer,
      };
    }

    case t.CLEAR_OFFERS_SUCCESS: {
      return {
        ...state,
        offersSuccess: false,
      };
    }

    default: {
      return state;
    }
  }
}
