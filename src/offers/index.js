import * as components from './components';
import * as actions from './actions';
import reducer from './reducer';
import * as constants from './constants';
import * as selectors from './selectors';

export default { actions, components, constants, reducer, selectors };
