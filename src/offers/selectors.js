import { createSelector } from 'reselect';
import { NAME } from './constants';
import { includes } from '../utils/functions';
import { clientsListSelector } from '../clients/selectors';
import { productsListSelector } from '../products/selectors';

const offersStateSelector = state => state[NAME];

export const clientsSelectSelector = createSelector(
  clientsListSelector,
  clientsList => clientsList.map(client => ({ label: client.name, value: client.id })),
);

export const productsSelectSelector = createSelector(
  productsListSelector,
  productsList => productsList.map(p => ({ label: p.name, value: p.id })),
);

export const offersSelector = createSelector(
  offersStateSelector,
  offersState => offersState.offers,
);

export const offerAddSuccessSelector = createSelector(
  offersStateSelector,
  offersState => offersState.offersSuccess,
);

export const offerDetailsSelector = createSelector(
  offersStateSelector,
  offersState => offersState.detailOffer,
);
