import React from 'react';
import Select from 'react-select';
import { connect } from 'react-redux';
import { getClients } from '../../../clients/actions';
import { clientsSelectSelector } from '../../selectors';
import AddClients from '../../../clients/components/AddClients';

type Props = {
  clients: Array<Object>;
  selectedClient: ?number;
  selectClient: (id: number) => void;
  getClients: () => void;
}

type State = {
  clientFormOpen: boolean;
}

class ClientSelect extends React.Component {
  props: Props;
  state: State;

  constructor(props: Props) {
    super(props);

    this.state = { clientFormOpen: false };
  }

  componentWillMount() {
    this.props.getClients();
  }

  render() {
    return (
      <div className="add-offer-client">
        <div>
          <Select
            className="select-list2"
            options={this.props.clients}
            value={this.props.selectedClient}
            onChange={id => this.props.selectClient(id)}
            searchable
            clearable={false}
            autoBlur
          />

          <span
            className="client-not-found"
            onClick={() => this.setState({ clientFormOpen: true })}
          >
            Ei leidnud klienti? Lisa uus
          </span>
        </div>

        {this.state.clientFormOpen &&
          <AddClients onSave={() => this.setState({ clientFormOpen: false })} />
        }
      </div>
    );
  }
}

const mapState = state => ({
  clients: clientsSelectSelector(state),
});

const mapDispatch = dispatch => ({
  getClients: () => dispatch(getClients()),
});

export default connect(mapState, mapDispatch)(ClientSelect);