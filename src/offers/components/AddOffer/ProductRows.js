import React from 'react';
import { ProductDetails } from '../../../products/components';

type Props = {
  products: Array<Object>;
  removeProduct: ?(id: number) => void;
  noDetails: boolean;
}

type State = {
  rowsOpened: Array<number>;
}

class ProductRows extends React.Component {
  props: Props;
  state: State;

  constructor(props: Props) {
    super(props);

    this.state = { rowsOpened: [] };
  }

  toggleRow = (rowNr: Number) => {
    if (this.props.noDetails) {
      return;
    }
    let { rowsOpened } = this.state;

    if (rowsOpened.includes(rowNr)) {
      rowsOpened = rowsOpened.filter(item => item !== rowNr);
    } else {
      rowsOpened.push(rowNr);
    }

    this.setState({ rowsOpened });
  }

  render() {
    const { products, removeProduct } = this.props;
    console.log(products);
    const { rowsOpened } = this.state;

    return (
      <div className="product-rows list-table">
        <table>
          <thead>
            <tr>
              <th>Nimetus</th>
              <th>KOGUS</th>
              <th>SUMMA</th>
            </tr>
          </thead>
          {products.map((p, rowNr) => (
            <tbody>
              <tr onClick={() => this.toggleRow(rowNr)}>
                <td>
                  {p.name}
                </td>
                <td>
                  {p.quantity}
                </td>
                <td>
                  {p.price}
                </td>
                {removeProduct &&
                  <td onClick={() => removeProduct(p.id)}>
                    Eemalda
                  </td>
                }
              </tr>
              {rowsOpened.includes(rowNr) &&
                <tr>
                  <td colSpan="3">
                    <ProductDetails product={p} />
                  </td>
                </tr>
              }
            </tbody>
            ))}
        </table>
      </div>
    );
  }
}

export default ProductRows;
