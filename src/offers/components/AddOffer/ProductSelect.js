import React from 'react';
import Select from 'react-select';
import { FormGroup, ControlLabel, FormControl, Checkbox } from 'react-bootstrap';
import { connect } from 'react-redux';
import { productsSelectSelector } from '../../selectors';
import { productsListSelector } from '../../../products/selectors';

import { getProducts } from '../../../products/actions';

type Props = {
  productsForSelect: Array<Object>;
  products: Array<Object>;
  addProduct: (product: Object, quantity: number, sum: number) => void;
  getProducts: () => void;
}

type State = {
  productFormOpen: boolean;
  selectedProduct: ?number;
  quantity: ?number;
  totalPrice: number;
  itemPrice: number,
}

class ProductSelect extends React.Component {
  props: Props;
  state: State;

  constructor(props: Props) {
    super(props);

    this.state = {
      productFormOpen: false,
      selectedProduct: {},
      quantity: null,
      totalPrice: 0,
      itemPrice: 0,
    };
  }

  componentWillMount() {
    this.props.getProducts();
  }

  changeProduct = (id: number) => {
    const product = this.props.products.find(p => p.id === id);
    this.setState({ selectedProduct: product });
  }

  addProduct = () => {
    const { quantity, totalPrice, selectedProduct } = this.state;
    this.props.addProduct(selectedProduct, quantity, totalPrice);
  }

  validateQuantity = () => {
    const field = this.state.quantity;
    if (field === null || field === 0) {
      return null;
    }

    return field && field !== '' ? null : 'error';
  }

  handleFieldChange = (e: Event) => {
    this.setState({ [e.target.id]: e.target.value });

    if (e.target.id === 'quantity') {
      this.handleQuantityChange(e.target.value);
    }
  }

  handleQuantityChange = (q: number) => {
    const { selectedProduct } = this.state;
    this.setState({ totalPrice: q * selectedProduct.price });
  }

  selectDisabled = () => {
    const { selectedProduct, quantity } = this.state;
    return Object.keys(selectedProduct).length < 1 || quantity < 1;
  }


  render() {

    const selectDisabled = this.selectDisabled();

    return (
      <div className="add-offer-product">
        <div>
          <div>
            <label>Vali toode </label>
            <Select
              className="select-list2"
              options={this.props.productsForSelect}
              value={this.state.selectedProduct.id}
              onChange={p => this.changeProduct(p.value)}
              searchable
              clearable={false}
              autoBlur
            />
            <FormGroup controlId="itemPrice">
              <ControlLabel>Tüki hind</ControlLabel>
              <FormControl
                type="number"
                name="itemPrice"
                placeholder="Tüki hind"
                onChange={this.handleFieldChange}
                value={this.state.selectedProduct.price}
              />
            </FormGroup>
          </div>
          <div style={{ marginLeft: '40px' }}>
            <FormGroup controlId="quantity" validationState={this.validateQuantity()}>
              <ControlLabel>Kogus</ControlLabel>
              <FormControl
                type="number"
                name="quantity"
                placeholder="Kogus"
                onChange={this.handleFieldChange}
                value={this.state.quantity}
              />
            </FormGroup>
            <div>
              <ControlLabel>Summa:</ControlLabel>
              <div>
                <input
                  type="number"
                  name="totalPrice"
                  id="totalPrice"
                  onChange={this.handleFieldChange}
                  value={this.state.totalPrice}
                />
              </div>
            </div>
          </div>
          <button onClick={this.addProduct} disabled={selectDisabled}>
            Lisa rida
          </button>
        </div>
      </div>
    );
  }
}

const mapState = state => ({
  productsForSelect: productsSelectSelector(state),
  products: productsListSelector(state),
});

const mapDispatch = dispatch => ({
  getProducts: () => dispatch(getProducts()),
});

export default connect(mapState, mapDispatch)(ProductSelect);
