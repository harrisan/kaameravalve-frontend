// @flow

import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { getOfferDetails, sendOutOffer } from '../actions';
import { OFFER_STATUS_SENT } from '../constants';
import { offerDetailsSelector } from '../selectors';
import { offerDownloadUrl } from '../../utils/functions';
import ProductRows from './AddOffer/ProductRows';

type Props = {
  detailOffer: Object;
  getOfferDetails: (id: number) => void;
  match: {
    params: Object,
  };
}

class DetailView extends React.Component {
  props: Props;

  componentWillMount = () => {
    if (this.props.match && this.props.match.params.id) {
      this.props.getOfferDetails(this.props.match.params.id);
    }
  }

  downloadOffer = () => {

  }

  render() {
    const { detailOffer, sendOutOffer } = this.props;
    const { client, products } = detailOffer;

    if (!Object.keys(detailOffer).length) {
      return <div>Pakkumist ei leitud</div>;
    }

    const backText = '< Tagasi nimekirja';

    return (
      <div className="offer-details">
        <Link className="back-to-list" to="/offers/list">
          {backText}
        </Link>
        <h2>
          Pakkumine nr.{detailOffer.id}
        </h2>
        <div className="offer-details-info">
          <div className="offer-details-info-offer">
            <h3>
              Pakkumise andmed
            </h3>
            <p>Loodud: <span>{detailOffer.createdAt}</span></p>
            <p>Staatus: <span>{detailOffer.status}</span></p>
            {detailOffer.status === OFFER_STATUS_SENT &&
              <p>Väljasaadetud: <span>{detailOffer.publishedAt}</span></p>
            }
            <p>Kogusumma: <span>{detailOffer.totalSum}</span></p>
            <p>Käibemaks: <span>{detailOffer.totalSum - detailOffer.totalSumWoVat}</span></p>
            <p>Summa käibemaksuta: <span>{detailOffer.totalSumWoVat}</span></p>
          </div>
          <div className="offer-details-info-client">
            <h3>
              Kliendi andmed
            </h3>
            <p>Nimi: <span>{client.name}</span></p>
            <p>Firma nimi: <span>{client.companyName}</span></p>
            <p>Email: <span>{client.email}</span></p>
            <p>Telefon: <span>{client.phone}</span></p>
            <button><a href={offerDownloadUrl(detailOffer.id)}>Lae pakkumine alla</a></button>
            <button onClick={() => sendOutOffer(detailOffer.id)}>Saada välja</button>
          </div>
        </div>
        <hr />
        <div className="offer-details-products">
          <h3>
            Pakkumises olevad tooted
          </h3>
          <ProductRows
            products={products}
          />
        </div>
      </div>
    );
  }
}

const mapState = state => ({ detailOffer: offerDetailsSelector(state) });

const mapDispatch = dispatch => ({
  getOfferDetails: id => dispatch(getOfferDetails(id)),
  sendOutOffer: id => dispatch(sendOutOffer(id)),
});

export default connect(mapState, mapDispatch)(DetailView);
