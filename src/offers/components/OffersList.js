// @flow

import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { getOffers } from '../actions';
import { offersSelector } from '../selectors';
import ProductRows from './AddOffer/ProductRows';

require('../styles/OffersList.scss');

type Props = {
  getOffers: () => void;
  offers: Array<Object>;
};

class OffersList extends React.Component {
  props: Props;

  componentWillMount = () => {
    this.props.getOffers();
  };

  render() {
    const { offers } = this.props;

    return (
      <div className="list-table offers-list">
        <h2>Kõik pakkumised</h2>
        <table>
          <thead>
            <th>
              Kliendi nimi
            </th>
            <th>
              Kogusumma
            </th>
            <th>
              Kuupäev
            </th>
            <th>
              Staatus
            </th>
          </thead>
          <tbody>
            {offers.length > 0 && offers.map(offer => (
              <tr className="offers-list-row">
                <td>
                  <Link
                    to={`/offer/${offer.id}`}
                  >
                    {offer.client.name}
                  </Link>
                </td>
                <td>
                  <Link
                    to={`/offer/${offer.id}`}
                  >
                    {offer.totalSum}
                  </Link>
                </td>
                <td>
                  <Link
                    to={`/offer/${offer.id}`}
                  >
                    {offer.createdAt}
                  </Link>
                </td>
                <td>
                  <Link
                    to={`/offer/${offer.id}`}
                  >
                    {offer.status === 'ready' ? 'Valmis saatmiseks' : 'Saadetud'}
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        <button><Link to="/offers/add">Lisa uus pakkumine</Link></button>
      </div>
    );
  }
}

const mapState = state => ({ offers: offersSelector(state) });

const mapDispatch = dispatch => ({ getOffers: () => dispatch(getOffers()) });

export default connect(mapState, mapDispatch)(OffersList);
