// @flow

import React from 'react';
import { connect } from 'react-redux';
import ClientSelect from './AddOffer/ClientSelect';
import ProductSelect from './AddOffer/ProductSelect';
import ProductRows from './AddOffer/ProductRows';
import { addOffer, clearOffersSuccess } from '../actions';
import { offerAddSuccessSelector } from '../selectors';

require('../styles/AddOffer.scss');

type Props = {
  addOffer: (offer: Object) => void;
  clearOffersSuccess: () => void;
  offerAddSuccess: boolean;
};

type State = {
  selectedClient: ?number;
  products: Array<Object>;
};

class AddOffer extends React.Component {
  props: Props;
  state: State;

  constructor(props: Props) {
    super(props);

    this.state = {
      selectedClient: null,
      products: [],
    };
  }

  selectClient = (client: number) => {
    this.setState({ selectedClient: client.value });
  }

  addProduct = (product: Object, quantity: number, sum: number) => {
    const existingProducts = this.state.products;
    const newProduct = { ...product, quantity, sum };
    existingProducts.push(newProduct);

    this.setState({ products: existingProducts });
  }

  removeProduct = (id: number) => {
    const products = this.state.products.filter(p => p.id !== id);

    this.setState({ products });
  }

  addOffer = () => {
    const newOffer = {
      clientId: this.state.selectedClient,
      products: this.state.products,
    };

    this.props.addOffer(newOffer);
  }

  storeInitialState = () => {
    this.props.clearOffersSuccess();
    this.setState({
      selectedClient: null,
      products: [],
    });
  }

  render() {
    const { selectedClient, products } = this.state;
    const { offerAddSuccess } = this.props;

    return (
      <div className="add-offer">
        <h2>Lisa uus pakkumine</h2>
        <h3>Vali klient</h3>
        <ClientSelect
          selectedClient={selectedClient}
          selectClient={this.selectClient}
        />

        {selectedClient && products.length > 0 &&
          <ProductRows
            products={products}
            removeProduct={this.removeProduct}
            noDetails
          />
        }
        {selectedClient &&
          <div className="add-offer-product">
            <h3>Lisa toode</h3>
            <ProductSelect addProduct={this.addProduct} />
          </div>
        }

        {selectedClient && products.length > 0 && !offerAddSuccess &&
          <button onClick={() => this.addOffer()} type="submit">Salvesta</button>
        }
        {offerAddSuccess &&
          <span
            onClick={() => this.storeInitialState()}
            className="add-clients-success"
          >
            Pakkumise lisamine õnnestus! Lisa uus?
          </span>
        }
      </div>
    );
  }
}


const mapState = state => ({
  offerAddSuccess: offerAddSuccessSelector(state),
});

const mapDispatch = dispatch => ({
  addOffer: offer => dispatch(addOffer(offer)),
  clearOffersSuccess: () => dispatch(clearOffersSuccess()),
});

export default connect(mapState, mapDispatch)(AddOffer);
