// @flow

export const includes = (text: ?string, key: string) => {
  if (!text) return false;
  return text.toUpperCase().indexOf(key.toUpperCase()) !== -1;
};

// Used with redux-api-middleware action creators.
export const headers = () => {
  const hash = btoa('kaameravalvedev:adminkaamera');

  return ({
    Authorization: `Basic ${hash}`,
  });
};

export const offerDownloadUrl = (id: string) => (
  `${API_URL}/offers/download/${id}`
);
