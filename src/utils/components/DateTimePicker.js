// @flow

import React, { Component } from 'react';
import Datetime from 'react-datetime';
import onClickOutside from 'react-onclickoutside';
import moment from 'moment';

// These are needed for react-datetime locale
require('moment/locale/et');
require('./Datetime.scss');

type Props = {
  dateTimeChanged: (date: moment | string) => void;
  closeOnSelect: boolean,
  timeFormat: ?string;
  dateFormat: ?string;
  dateTime: ?Object;
  timeConstraints: Object;
  inputProps: ?Object;
  strictParsing: ?boolean;
  locale: string;
  allowNullDate: ?boolean;
  endDateValidation: (endDate: moment, startDate: moment) => void;
  startDate: moment;
  noDateValidation: ?boolean;
  customValidator: (date: moment) => boolean;
}

type State = {
  open: boolean;
}

type DefaultProps = {
  timeConstraints: Object;
}

class DateTimePicker extends Component {
  static defaultProps: DefaultProps;

  constructor(props: Props) {
    super(props);

    this.state = {
      open: false,
    };

    this.handleChange = this.handleChange.bind(this);
  }

  state: State;
  props: Props;
  handleChange: (date: moment | string) => void;

  handleClickOutside() {
    this.setState({ open: false });
  }

  handleChange(dateTime: moment | string) {
    if ((dateTime instanceof moment) && this.props.dateTimeChanged) {
      this.props.dateTimeChanged(dateTime);
      if (this.props.dateFormat) {
        this.setState({ open: false });
      }
    }
  }

  renderInput = (props: Object) => (
    <div>
      <div
        onClick={() => this.setState({ open: !this.state.open })}
        className="date-time-picker-custom-input"
      >
        {this.props.label &&
          <label className="date-time-input-label">{this.props.label}</label>
        }
        <input
          {...props}
          onFocus={() => document.activeElement && document.activeElement.blur()}
        />
        <span className="indicator" />
      </div>
    </div>
  );

  render() {
    // Set timeFormat prop to 'H:mm' or eqv. to enable selecting time
    const {
      closeOnSelect = true,
      timeFormat = 'HH:MM',
      dateFormat = 'DD/MM/YYYY',
      inputProps = { readOnly: true },
      strictParsing = true,
      timeConstraints,
      dateTime,
    } = this.props;

    const { open } = this.state;

    return (
      <Datetime
        className="date-time-picker"
        dateFormat={dateFormat}
        timeFormat
        value={dateTime}
        strictParsing={strictParsing}
        onChange={this.handleChange}
        timeConstraints={timeConstraints}
        locale="et"
        closeOnSelect={closeOnSelect}
        open={open}
        renderInput={this.renderInput}
      />
    );
  }
}

export default onClickOutside(DateTimePicker);
