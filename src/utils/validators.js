// @flow

const isString = (value: mixed): boolean %checks => (
  typeof value === 'string'
);

export const phoneValid = (phone: mixed): boolean %checks => (
  isString(phone) && /^\+?([0-9]{3,15})$/.test(phone)
);

export const emailValid = (email: mixed): boolean %checks => (
  isString(email) && /^[a-zA-Z0-9._+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(email)
);

export const passwordValid = (password: mixed): boolean %checks => (
  isString(password) && /^[A-Za-z0-9]{8,}$/.test(password)
);

export const emptyString = (value: mixed): boolean %checks => (
  !isString(value) || value.length === 0
);

export const notEmpty = (value: mixed): boolean %checks => (
  !emptyString(value)
);
