const path = require('path');
const express = require('express');
const webpack = require('webpack');
const devMiddleware = require('webpack-dev-middleware');
const hotMiddleware = require('webpack-hot-middleware');
const config = require('./webpack.config.dev');

const app = new express();
const port = 3001;
const configObj = typeof config === "function" ? config() : config;
const compiler = webpack(configObj);

app.use(devMiddleware(compiler, { publicPath: configObj.output.publicPath }));
app.use(hotMiddleware(compiler));

app.get('*', (req, res, next) => {
  const filename = path.join(compiler.outputPath, 'index.html');
  compiler.outputFileSystem.readFile(filename, function(err, result) {
    if (err) {
      return next(err);
    }

    res.set('Content-Type', 'text/html');
    res.send(result);
    res.end();
  });
});

app.listen(port, error => {
  if (error) {
    console.error(error);
  } else {
    console.info('==> Listening on port %s. Open up http://localhost:%s/ in your browser.', port, port);
  }
});
