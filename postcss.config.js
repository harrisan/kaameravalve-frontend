module.exports = {
  syntax: 'postcss-scss',
  plugins: {
    'postcss-url': [
      {
        url: 'inline',
        filter: '**/src/**/*.svg',
      },
    ],
    autoprefixer: {
      browsers: [
        '> 1%',
        'last 2 versions',
        'IE 9',
      ],
    },
  },
};
