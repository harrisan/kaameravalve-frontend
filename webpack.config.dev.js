var webpack = require('webpack');
var path = require('path');
const HTMLPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const config = {
  dev: {
    /*API_URL: "'http://127.0.0.1:8000/api'",
    AUTH_API_URL: "'http://127.0.0.1:8000/oauth/token'",*/
    API_URL: "'http://api2.hriconsult.eu/api'",
    AUTH_API_URL: "'http://api2.hriconsult.eu/oauth/token'",
    APPLICATION_ID: "'2'",
    APPLICATION_SECRET: "'551wA9kAdzcZjNwGQvqivTTQK7oEcf0MkTVJ2zuF'",
    HTACCESS: '.htaccess.example',
  },

  staging: {
    API_URL: "'http://api2.hriconsult.eu/api'",
    AUTH_API_URL: "'http://api2.hriconsult.eu/oauth/token'",
    APPLICATION_ID: "'2'",
    APPLICATION_SECRET: "'551wA9kAdzcZjNwGQvqivTTQK7oEcf0MkTVJ2zuF'",
    HTACCESS: '.htaccess.hriconsult',
  },

  production: {
    API_URL: "'http://127.0.0.1:8000/api'",
    AUTH_API_URL: "'http://127.0.0.1:8000/oauth/token'",
    APPLICATION_ID: "'2'",
    APPLICATION_SECRET: "'nuCq13IL9cKGry91gdq7Tz9jbr1O05JoTIrSGDnm'",
    HTACCESS: '.htaccess.kaameravalve',
  }
};

const hotLoaderEntrypoints = [
  'react-hot-loader/patch',
  'webpack-hot-middleware/client',
];

const devPlugins = [
  new webpack.HotModuleReplacementPlugin(),
];

const optimizePlugins = [
  new webpack.LoaderOptionsPlugin({
    minimize: true,
    debug: false
  }),
  new webpack.optimize.UglifyJsPlugin({
    compress: {
      dead_code: true,
      conditionals: true,
      booleans: true,
      unused: true,
      if_return: true,
      join_vars: true,
      drop_console: true
    },
    output: {
      comments: false
    }
  }),
];

module.exports = function(env) {
  env = env || 'dev';

  const isDev = env === 'dev';
  const isOptimized = !isDev;

  return {
    entry: [
      'babel-polyfill',
      './src/index',
    ],

    output: {
      path: path.join(__dirname, 'dist'),
      filename: 'bundle.js',
      publicPath: '/static/',
    },

    plugins: [
      new webpack.DefinePlugin(Object.assign({}, config.common, config[env])),
      new HTMLPlugin({
        inject: false,
        template: './src/index.html.template.dev',
      }),

      ...(isDev ? devPlugins : []),
      ...(isOptimized ? optimizePlugins : []),
    ],

    module: {
      loaders: [
        {
          test: /\.js$/,
          loaders: ['babel-loader'],
          exclude: /node_modules/,
          include: __dirname
        },
        {
          test: /flickity/,
          loader: 'imports?define=>false&this=>window'
        },
        {
          test: /\.woff(2)?(\?v=[0-9].[0-9].[0-9])?$/,
          loader: "url-loader?mimetype=application/font-woff"
        },
        {
          test: /\.(ttf|eot|svg|gif|jpg|pdf)(\?v=[0-9].[0-9].[0-9])?$/,
          loader: "file-loader?name=[name].[ext]"
        },
        {
          test: /\.png$/,
          loader: 'url-loader?limit=2750&name=[name].[ext]&mimetype=image/png'
        },
        {
          test: /\.scss$/,
          loader: ['style-loader', 'css-loader', 'sass-loader'],
        },

      ],
    }
  }
}
