const webpack = require('webpack');
const path = require('path');
const HTMLPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const config = {
  dev: {
    API_URL: "'http://127.0.0.1:8000/api'",
    AUTH_API_URL: "'http://127.0.0.1:8000/oauth/token'",
    APPLICATION_ID: "'1'",
    APPLICATION_SECRET: "'3EH7TKujqSk9rJrKXLeswapPg26ReyE3AX6XT91g'",
    HTACCESS: '.htaccess.example',
  },

  staging: {
    API_URL: "'http://api2.hriconsult.eu/api'",
    AUTH_API_URL: "'http://api2.hriconsult.eu/oauth/token'",
    APPLICATION_ID: '2',
    APPLICATION_SECRET: "'551wA9kAdzcZjNwGQvqivTTQK7oEcf0MkTVJ2zuF'",
    HTACCESS: '.htaccess.hriconsult',
  },

  production: {
    API_URL: "'http://127.0.0.1:8000/api'",
    AUTH_API_URL: "'http://127.0.0.1:8000/oauth/token'",
    APPLICATION_ID: "'2'",
    APPLICATION_SECRET: "'nuCq13IL9cKGry91gdq7Tz9jbr1O05JoTIrSGDnm'",
    HTACCESS: '.htaccess.kaameravalve',
  }
};

const optimizePlugins = [
  /*new webpack.LoaderOptionsPlugin({
    minimize: true,
    debug: false,
  }),*/
  new webpack.optimize.UglifyJsPlugin({
    sourceMap: true,
    minimize: true,
    comments: false,
  }),
];

module.exports = function (e) {
  let env = 'dev';
  if (e && e.NODE_ENV) {
    env = e.NODE_ENV;
  }

  return {
    entry: [
      'babel-polyfill',
      './src/index',
    ],

    output: {
      path: path.join(__dirname, 'dist'),
      filename: 'bundle.[hash].js',
      publicPath: '/',
    },
    devtool: 'source-map',
    plugins: [
      ...optimizePlugins,
      new CopyWebpackPlugin([
        {
          from: config[env].HTACCESS,
          to: `${path.join(__dirname, 'dist')}/.htaccess`,
          force: true,
          toType: 'file',
        },
        { from: '.htpasswd', force: true },
      ]),
      new webpack.DefinePlugin(Object.assign({}, config.common, config[env])),
      new ExtractTextPlugin('styles.[hash].css'),
      new HTMLPlugin({
        filename: 'index.html',
        template: './src/index.html.template',
      }),
    ],

    module: {
      loaders: [
        {
          test: /\.js$/,
          loaders: ['babel-loader'],
          exclude: /node_modules/,
          include: __dirname,
        },
        {
          test: /flickity/,
          loader: 'imports?define=>false&this=>window'
        },
        {
          test: /\.woff(2)?(\?v=[0-9].[0-9].[0-9])?$/,
          loader: "url-loader?mimetype=application/font-woff"
        },
        {
          test: /\.(ttf|eot|gif|jpg|pdf)(\?v=[0-9].[0-9].[0-9])?$/,
          loader: "file-loader?name=[name].[ext]"
        },
        {
          test: /\.png$/,
          loader: 'url-loader?limit=2750&name=[name].[ext]&mimetype=image/png'
        },
        {
          test: /\.svg(\?v=[0-9].[0-9].[0-9])?$/,
          exclude: /node_modules/,
          loader: 'svg-url-loader?noquotes',
        },
        {
          test: /\.svg(\?v=[0-9].[0-9].[0-9])?$/,
          include: /node_modules/,
          loader: 'file-loader?name=[name].[ext]',
        },
        {
          test: /\.scss$/,
          loader: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
              'css-loader',
              'postcss-loader',
              'sass-loader',
            ],
          }),
        },
      ],
    }
  }
}
